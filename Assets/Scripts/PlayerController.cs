using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

public class PlayerController : MonoBehaviour 
{
    public GameObject BodySourceManagerGo;
    public GameObject CreatureGo;
    public GameObject GUIManagerGo;
    public float Speed;
    public float Threshold;
    public Boundary Boundary;
    public Animator Anim;

    private bool _isFighting = false;
    private bool _isLaunchingAttack = false;
    private GUIManager _guiManager;
    private bool _facingRight = true;
    private BodySourceManager _bodySourceManager;
    private float _angleArmShoulderInitial = 0f;
    private float _gestureSum = 0f;
    private float _step;
    private List<int> _degreePosition;
    private bool _isReturn = false;
    private Vector3 _target;

    void Awake()
    {
        _guiManager = GUIManagerGo.GetComponent<GUIManager>();
        Anim = GetComponent<Animator>();
        _bodySourceManager = BodySourceManagerGo.GetComponent<BodySourceManager>();
    }

	void Start () 
    {
        _target = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        _degreePosition = new List<int>();
        _degreePosition.Add(0);
	}
	
	void Update () 
    {
        //Recupere le premier utilisateur synchronise
        Body[] data = _bodySourceManager.GetData();
        Body user = _bodySourceManager.getFirstTrackedUser();

        if (user == null) { return; }

        //Calcul des axes du plan referentiel du torse
        Vector3 right = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                        user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 up = user.Joints[JointType.SpineShoulder].GetJointPosition() -
                     user.Joints[JointType.SpineBase].GetJointPosition();
        Vector3 forward = Vector3.Cross(right, up); //Left Hand Rule

        //Calcul de la direction du bras
        Vector3 armRight = user.Joints[JointType.ShoulderRight].GetJointPosition() -
                           user.Joints[JointType.ElbowRight].GetJointPosition();
        
        //Calcul de la direction de l'avant bras
        Vector3 forearmRight = user.Joints[JointType.HandRight].GetJointPosition() -
                          user.Joints[JointType.ElbowRight].GetJointPosition();

        //Calcul de l'angle initial entre le bras collé à son flanc et la direction des épaules
        if(_angleArmShoulderInitial == 0)
            _angleArmShoulderInitial = Vector3.Angle(armRight, right);

        //Calcul de l'angle entre l'avant-bras et le bras
        float angleForearmArm = Vector3.Angle(forearmRight, armRight);

        //Calcul de l'angle courant entre le bras et la direction des épaules
        float angleArmShoulderCurrent = Vector3.Angle(armRight, right);

        //Calcul de l'angle entre la direction des épaules et de l'avant bras
        float angleForearmShoulder = Vector3.Angle(forearmRight, right);
        if (!_isFighting && !_isLaunchingAttack)
        {
            //Test si l'utilisateur plaque bien son bras contre son flanc
            if (angleArmShoulderCurrent > _angleArmShoulderInitial - 15 && angleArmShoulderCurrent < _angleArmShoulderInitial + 15)
            {
                _guiManager.StopDisplayAvatar();
                //Test si l'avant bras est bien à angle droit du bras
                if (angleForearmArm > 75 && angleForearmArm < 105)
                {
                    //Test si l'utilisateur fait le bon mouvement avec son avant bras
                    if (angleForearmShoulder > 0 && angleForearmShoulder < 100)
                    {
                        /*if ((_degreePosition[_degreePosition.Count-1] < angleForearmShoulder))
                        {
                            _degreePosition.Add((int)angleForearmShoulder);
                            _gestureSum++;
                            Debug.Log("ANGLE FOREARMSHOULDER : " + (int)angleForearmShoulder + " GESTURE SUM : " + _gestureSum);
                        }*/

                        //Test si j'ai mon bras collé au ventre alors mon allez est ok
                        if ((angleForearmShoulder > 0 && angleForearmShoulder < 30) && !_isReturn /*&& _gestureSum >= Threshold*/)
                        {
                            /*Debug.Log("IS RETURN");
                            _degreePosition.Clear();
                            _degreePosition.Add(0);*/
                            _isReturn = true;
                            //_gestureSum = 0f;
                        }
                        else if ((angleForearmShoulder < 95 && angleForearmShoulder > 80) && _isReturn /*&& _gestureSum >= Threshold*/) //Test si je suis bien à angle droit de mon ventre et que j'ai bien fait mon allez
                        {
                            /*Debug.Log("! IS RETURN");
                            _degreePosition.Clear();
                            _degreePosition.Add(0);*/
                            _target = new Vector3(transform.position.x + 2, transform.position.y, transform.position.z); //Nouvelle position pour mon player 
                            //_gestureSum = 0f;
                            _isReturn = false;
                        }
                    }
                }
            }
            else 
                _guiManager.DisplayAvatar();
        }
        else if (_isFighting && !_isLaunchingAttack)
        {
            InvokeGesture(forearmRight, up, angleForearmArm, angleArmShoulderCurrent);
        }
            

        if (_target != Vector3.zero)
        {
            if (transform.position != _target)
            {
                Anim.SetFloat("Speed", Mathf.Abs(1f));
                transform.position = Vector3.MoveTowards(transform.position, _target, Speed * Time.deltaTime);
            }
            else
            {
                _target = Vector3.zero; 
                Anim.SetFloat("Speed", Mathf.Abs(0f));
            }
        }

        //Controle au clavier
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Anim.SetFloat("TimerInvoke", 1f);
        }
        else if (Input.GetKeyUp(KeyCode.Space))
            Anim.SetFloat("TimerInvoke", 0f);
	}

    void FixedUpdate()
    {
        float move = Input.GetAxis("Horizontal");
        Anim.SetFloat("Speed", Mathf.Abs(move));
        if (transform.position.x > Boundary.xMin || transform.position.x > Boundary.xMax || move < 0 || move > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(move * Speed, GetComponent<Rigidbody2D>().velocity.y);

            if (move > 0 && !_facingRight)
                Flip();
            else if (move < 0 && _facingRight)
                Flip();
        }

    }

    void Flip()
    {
        _facingRight = !_facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void InvokeGesture(Vector3 forearmRight, Vector3 up, float angleForearmArm, float angleArmShoulder)
    {
        //angleForearmArm  utile pour savoir si on a le bras tendu
        //angleArmShoulderCurrent // utile pour savoir si le bras est bien à 90degrès des épaules

        float _angleForearmUp = Vector3.Angle(forearmRight, up);

        if(angleArmShoulder > 80 && angleArmShoulder < 100)
        {
            if(angleForearmArm > 160 && angleForearmArm < 190)
            {
                if(_angleForearmUp > 80 && _angleForearmUp < 90)
                {
                    _guiManager.FillMana();
                    Anim.SetFloat("TimerInvoke", Mathf.Abs(1f));
                }
            }
        }

        if(_guiManager.ManaIsFilling())
        {
            Anim.SetFloat("TimerInvoke", Mathf.Abs(0f));
            CreatureGo.SetActive(true);
            _guiManager.ManaImg.fillAmount = 0f;
            _isFighting = false;
            _isLaunchingAttack = true;
        }
    }

    public void SetIsFigthing(bool isFighting)
    {
        _isFighting = isFighting;
    }
}
