using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour 
{
    public Image AvatarImg;
    public Image ManaImg;
    public Text MessageTxt;
    public float ManaSpeed = 0.1f;

	void Start () 
    {
        AvatarImg.gameObject.SetActive(false);
        MessageTxt.gameObject.SetActive(false);

        ManaImg.fillAmount = 0f;

	}
	
	void Update () 
    {
	
	}

    public void DisplayAvatar()
    {
        AvatarImg.gameObject.SetActive(true);
        MessageTxt.gameObject.SetActive(true);
    }

    public void StopDisplayAvatar()
    {
        AvatarImg.gameObject.SetActive(false);
        MessageTxt.gameObject.SetActive(false);
    }

    public void FillMana()
    {
        ManaImg.fillAmount += ManaSpeed;
    }

    public bool ManaIsFilling()
    {
        return ManaImg.fillAmount == 1f;
    }
}
