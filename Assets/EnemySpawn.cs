using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour 
{
    public GameObject PlayerGo;
    public GameObject EnemyGo;

    private PlayerController _playerController;

    void Awake()
    {
        _playerController = PlayerGo.GetComponent<PlayerController>();
    }

	void Start () 
    {
        EnemyGo.SetActive(false);
	
	}
	
	void Update () 
    {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            EnemyGo.SetActive(true);
            _playerController.SetIsFigthing(true);
        }
    }
}
